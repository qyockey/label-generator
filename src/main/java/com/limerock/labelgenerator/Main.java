package com.limerock.labelgenerator;

import static com.limerock.labelgenerator.Constants.CellType.IMAGE_CELL;
import static com.limerock.labelgenerator.Constants.CellType.QR_CELL;
import static com.limerock.labelgenerator.Constants.CellType.TEXT_CELL;
import static com.limerock.labelgenerator.Constants.CELL_TYPES;
import static com.limerock.labelgenerator.Constants.FONTS;
import static com.limerock.labelgenerator.Constants.PAGE_SIZES;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.limerock.labelgenerator.Constants.CellType;
import com.limerock.labelgenerator.SpreadsheetFormat.Extension;

public class Main {
  
  public static final File TEMPLATE_DIR = new File("label-templates/");

  static {
    System.out.println("\n                  Lime Rock Label Generator\n");
    if (!TEMPLATE_DIR.exists() && !TEMPLATE_DIR.mkdirs()) {
      System.out.println("Error: failed to create template directory");
      Input.exit(1);
    }
  }

  public static void main(final String[] args) {
    File spreadsheetFile;
    Extension extension;
    while (true) {
      final String spreadsheetFilepath = Input.readString("Enter spreadsheet file path: ");
      spreadsheetFile = new File(spreadsheetFilepath);
      if (spreadsheetFile.exists()) {
        extension = Parser.getExtension(spreadsheetFilepath);
        if (extension == null) {
          System.out.printf("Unsupported file extension %s%n", extension);
        } else {
          break;
        }
      } else {
        System.out.printf("Error: file not found at %s%n", spreadsheetFile.getAbsolutePath());
      }
    }
    final SpreadsheetFormat spreadsheetFormat = new SpreadsheetFormat()
        .setExtension(extension)
        .setHasHeader(Input.readBool("Does the spreadsheet have a header? [y/n] "));
    if (extension.equals(Extension.CSV)) {
      spreadsheetFormat.setDelimiter(Input.readString("CSV file detected, enter delimiter: "));
    }

    final LabelFormat labelFormat;
    File[] templateDirFiles = TEMPLATE_DIR.listFiles();
    if (templateDirFiles == null) {
      templateDirFiles = new File[0];
    }
    final Set<String> cfgFilenames = Arrays.asList(templateDirFiles)
        .stream()
        .filter(file -> file.getName().endsWith(".cfg"))
        .map(file -> file.getName().replace(".cfg", ""))
        .collect(Collectors.toSet());
    if (!cfgFilenames.isEmpty() && 
        Input.readBool("Import a template file? [y/n] ")) {
      File templateFile;
      while (true) {
        final String templateName = Input.readStringFromOptions(
            "Enter template name: ", cfgFilenames);
        templateFile = new File(TEMPLATE_DIR, templateName + ".cfg");
        if (templateFile.exists()) {
          break;
        } else {
          System.out.printf("Error: template \"%s\" not found%n", templateName);
          System.out.println("Available templates:");
          cfgFilenames.stream()
              .forEach(filename -> System.out.println("  " + filename));
        }
      }
      labelFormat = new LabelFormat(templateFile);
    } else {
      final int rowsPerEntry = Input.readInt("Number of rows per entry: ");
      final int colsPerEntry = Input.readInt("Number of columns per entry: ");
      final EntryCell[][] cellLayout = new EntryCell[rowsPerEntry][colsPerEntry];
      for (int row = 0; row < rowsPerEntry; row++) {
        for (int col = 0; col < colsPerEntry; col++) {
          System.out.printf("%n                             Row %d Column %d%n",
              row + 1, col + 1);
          String cellTypeStr = Input.readStringFromOptions("Enter cell type: ",
              CELL_TYPES.keySet());
          CellType cellType = CELL_TYPES.get(cellTypeStr);
          EntryCell entryCell;
          switch (cellType) {
            case TEXT_CELL:
              List<TextCellLine> textCellLines = new ArrayList<>();
              int lineNum = 0;
              while (true) {
                System.out.printf("                                     Line %d%n",
                    lineNum + 1);
                textCellLines.add(new TextCellLine(
                  Input.readString("Enter content: "),
                  Input.readBool("Format bold? [y/n] "),
                  Input.readBool("Format Italic? [y/n] ")
                ));
                if (!Input.readBool("Add another line? [y/n] ")) {
                  break;
                }
                lineNum++;
              }
              entryCell = new EntryCell(TEXT_CELL, textCellLines.toArray(
                  new TextCellLine[0]));
              break;
            case QR_CELL:
              entryCell = new EntryCell(QR_CELL, Input.readString("URL to encode: "));
              break;
            case IMAGE_CELL:
              entryCell = new EntryCell(IMAGE_CELL, Input.readString("Image filepath: "));
              break;
            default: 
              entryCell = new EntryCell(cellType);
          }
          cellLayout[row][col] = entryCell;
        }
      }
      System.out.println();
      labelFormat = new LabelFormat()
          .setFont(Input.readStringFromOptions("Enter font: ", FONTS.keySet()))
          .setPageSize(Input.readStringFromOptions("Enter page size: ", PAGE_SIZES.keySet()))
          .setMarginWidth(Input.readFloat("Margin width (in): "))
          .setLandscapeMode(Input.readBool("Use landscape mode? [y/n] "))
          .setHasOutline(Input.readBool("Use table outlines? [y/n] "))
          .setEntriesPerRow(Input.readInt("Number of entries per row: "))
          .setEntriesPerCol(Input.readInt("Number of entries per column: "))
          .setPageWidthPercent(Input.readFloat("Percentage of page to use: "))
          .setImagePadding(Input.readFloat("Image padding (in): "))
          .setCellLayout(cellLayout);
      if (Input.readBool("Export label format? [y/n] ")) {
        do {
          final String templateName = Input.readString("Template name: ");
          final File cfgFile = new File(TEMPLATE_DIR, templateName + ".cfg");
          if (cfgFile.exists()) {
            if (Input.readBool("File already exists. Overwrite? [y/n] ")) {
              labelFormat.writeConfigFile(cfgFile);
              break;
            } else {
              if (!Input.readBool("Save with a different name? [y/n] ")) {
                break;
              }
            }
          } else {
            labelFormat.writeConfigFile(cfgFile);
            break;
          }
        } while (true);
      }
    }
    Parser.init(spreadsheetFile, spreadsheetFormat);
    final Set<Part> parts = Parser.parseParts();
    String pdfPath;
    while (true) {
      pdfPath = Input.readString("Output PDF file path: ");
      if (pdfPath.endsWith(".pdf")) {
        break;
      } else {
        System.out.println("PDF file path must end in .pdf");
      }
    }
    try {
      final PdfGenerator pdf = new PdfGenerator(pdfPath, labelFormat);
      Part[] partRow = new Part[labelFormat.getEntriesPerRow()];
      int partsInRow = 0;
      for (final Part part : parts) {
        if (partsInRow < partRow.length) {
          partRow[partsInRow++] = part;
        }
        if (partsInRow == partRow.length) {
          pdf.addPartRow(partRow);
          for (int partIndex = 0; partIndex < partRow.length; partIndex++) {
            partRow[partIndex] = null;
          }
          partsInRow = 0;
        }
      }
      if (partsInRow > 0) {
        pdf.addPartRow(partRow);
      }
      pdf.writeChanges();
      System.out.println("PDF file generated successfully!");
      Input.exit(0);
    } catch (FileNotFoundException e) {
      System.out.println("Error creating PDF file. Make sure PDF path ends in .pdf");
    }
  }

}
