package com.limerock.labelgenerator;

import java.util.Map;

public class Part {
  
  private final Map<String, String> fieldToAttribute;

  public Part(final Map<String, String> fieldToAttribute) {
    this.fieldToAttribute = fieldToAttribute;
  }

  public String getAttribute(final String field) {
    return fieldToAttribute.get(field);
  }

  public String toString() {
    return fieldToAttribute.values().toString();
  }

}
