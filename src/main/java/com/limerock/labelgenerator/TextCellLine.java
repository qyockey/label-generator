package com.limerock.labelgenerator;

public class TextCellLine {
  private final String rawData;
  private final boolean isBold;
  private final boolean isItalic;

  public TextCellLine(String rawData, boolean isBold, boolean isItalic) {
    this.isBold = isBold;
    this.isItalic = isItalic;
    this.rawData = rawData;
  }

  public String getRawData() {
    return rawData;
  }

  public boolean isBold() {
    return isBold;
  }

  public boolean isItalic() {
    return isItalic;
  }

}
