package com.limerock.labelgenerator;

import java.net.MalformedURLException;

import com.itextpdf.barcodes.BarcodeQRCode;
import com.itextpdf.io.exceptions.IOException;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.ListItem;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;

public class CellFactory {

  private CellFactory() {}

  public static Cell createEmptyCell(
      final float rowHeight,
      final float colWidth,
      final boolean hasOutline
  ) {
    final Cell emptyCell = new Cell()
        .setHeight(rowHeight)
        .setWidth(colWidth)
        .setTextAlignment(TextAlignment.CENTER)
        .setHorizontalAlignment(HorizontalAlignment.CENTER)
        .setVerticalAlignment(VerticalAlignment.MIDDLE);
    if (!hasOutline) {
      emptyCell.setBorder(Border.NO_BORDER);
    }
    return emptyCell;
  }

  public static Cell createTextCell(
      final TextCellLine[] lines,
      final Part part,
      final PdfFont font,
      final float rowHeight,
      final float colWidth,
      final boolean hasOutline
  ) {
    final Cell textCell = createEmptyCell(rowHeight, colWidth, hasOutline);
    final com.itextpdf.layout.element.List formattedLines = new com.itextpdf.layout.element.List()
        .setFont(font)
        .setListSymbol("")
        .setHorizontalAlignment(HorizontalAlignment.CENTER)
        .setVerticalAlignment(VerticalAlignment.MIDDLE)
        .setHeight(rowHeight)
        .setWidth(colWidth);
    for (TextCellLine line : lines) {
      final String formattedMessage = parseCellData(line.getRawData(), part);
      final ListItem lineItem = new ListItem(formattedMessage);
      if (line.isBold()) {
        lineItem.setBold();
      }
      if (line.isItalic()) {
        lineItem.setItalic();
      }
      formattedLines.add(lineItem);
    }
    textCell.add(formattedLines);
    return textCell;
  }

  public static Cell createQrCell(
      final String data,
      final Part part,
      final PdfDocument pdf,
      final float imagePadding,
      final float rowHeight,
      final float colWidth,
      final boolean hasOutline
  ) {
    final String url = parseCellData(data, part);
    final BarcodeQRCode qrCode = new BarcodeQRCode(url);
    final Image qrImage = new Image(qrCode.createFormXObject(
        ColorConstants.BLACK, pdf));
    return createImageCell(qrImage, imagePadding, rowHeight, colWidth, hasOutline);
  }

  public static Cell createImageCell(
      final String data,
      final Part part,
      final float imagePadding,
      final float rowHeight,
      final float colWidth,
      final boolean hasOutline
  ) {
    final String imageFilepath = parseCellData(data, part);
    Image productImage;
    try {
      productImage = new Image(ImageDataFactory.create(imageFilepath));
      return createImageCell(productImage, imagePadding, rowHeight, colWidth, hasOutline);
    } catch (MalformedURLException e) {
      System.out.printf("Error: File not found \"%s\"%n", imageFilepath);
      return createEmptyCell(rowHeight, colWidth, hasOutline);
    } catch (IOException e) {
      System.out.printf("Image file not found at filepath \"%s\"%n", imageFilepath);
      return createEmptyCell(rowHeight, colWidth, hasOutline);
    }
  }

  public static Cell createImageCell(
      final Image image,
      final float imagePadding,
      final float rowHeight,
      final float colWidth,
      final boolean hasOutline
  ) {
    image.scaleToFit(rowHeight - imagePadding, rowHeight - imagePadding)
        .setHorizontalAlignment(HorizontalAlignment.CENTER);
    final Cell imageCell = createEmptyCell(rowHeight, colWidth, hasOutline)
        .setWidth(rowHeight);
    final float cellHeight = imageCell.getHeight().getValue();
    final float cellWidth = imageCell.getWidth().getValue();
    final float imageHeight = image.getImageScaledHeight();
    final float imageWidth = image.getImageScaledWidth();
    final Border verticalBorder = new SolidBorder(ColorConstants.WHITE,
        (cellHeight - imageHeight) / 2);
    final Border horizontalBorder = new SolidBorder(ColorConstants.WHITE,
        (cellWidth - imageWidth) / 2);
    image
        .setBorderTop(verticalBorder)
        .setBorderBottom(verticalBorder)
        .setBorderLeft(horizontalBorder)
        .setBorderRight(horizontalBorder);
    return imageCell.add(image);
  }

  private static String parseCellData(final String data, final Part part) {
    StringBuilder bld = new StringBuilder();
    String[] tokens = data.split("\\$");
    boolean isVariable = false;
    for (String token : tokens) {
      if (isVariable) {
        String attribute = part.getAttribute(token);
        if (attribute == null) {
          System.out.printf("Field \"%s\" not found in spreadsheet header%n", 
              token);
          bld.append("");
        } else {
          bld.append(attribute);
        }
      } else {
        bld.append(token);
      }
      isVariable = !isVariable;
    }
    return new String(bld);
  }

}
