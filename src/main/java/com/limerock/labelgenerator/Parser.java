package com.limerock.labelgenerator;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.limerock.labelgenerator.SpreadsheetFormat.Extension;

public class Parser {
  
  private static File file;
  private static SpreadsheetFormat format;
  private static String[] header;

  private Parser() {}

  public static void init(final File file, final SpreadsheetFormat format) {
    Parser.file = file;
    Parser.format = format;
  }

  public static Extension getExtension(final String filename) {
    final String extension = filename.substring(filename.lastIndexOf('.'));
    switch (extension) {
      case ".xlsx":
        return Extension.XLSX;
      case ".xls":
        return Extension.XLS;
      case ".csv":
        return Extension.CSV;
      default:
        return null;
    }
  }

  public static Set<Part> parseParts() {
    Extension extension = getExtension(file.getName());
    if (extension != null) {
      switch (extension) {
        case CSV:  return parsePartsCsv(file);
        case XLS:  
        case XLSX: return parsePartsExcel(file, extension);
        default:   return Collections.emptySet();
      }
    } else {
      return Collections.emptySet();
    }

  }

  private static Set<Part> parsePartsCsv(final File csvFile) {
    final Set<Part> parts = new LinkedHashSet<>();
    try (final BufferedReader reader = new BufferedReader(new FileReader(csvFile))) {
      boolean isFirstRow = true;
      String line = reader.readLine();
      while (line != null) {
        String[] rowData = line.split(format.getDelimiter());
        if (isFirstRow) {
          final int numColumns = rowData.length;
          format.setNumColums(numColumns);
          if (format.hasHeader()) {
            header = rowData;
            line = reader.readLine();
            rowData = line.split(format.getDelimiter());
          } else {
            header = getDefaultHeader();
          }
          isFirstRow = false;
        }
        parts.add(parsePart(rowData));
        line = reader.readLine();
      }
    } catch (IOException e) {
      System.out.printf("Error: File not found \"%s\"%n", file.getAbsolutePath());
    }
    return parts;
  }

  private static Set<Part> parsePartsExcel(
      final File excelFile,
      final Extension extension
  ) {
    final Set<Part> parts = new LinkedHashSet<>();
    try (final FileInputStream inputStream = new FileInputStream(excelFile)) {
      final Workbook workbook;
      if (extension.equals(Extension.XLSX)) {
        workbook = new XSSFWorkbook(inputStream);
      } else { // extension == XLS
        workbook = new HSSFWorkbook(inputStream);
      }
      for (Sheet sheet : workbook) {
        boolean isFirstRow = true;
        int rowNum = 0;
        for (Row row : sheet) {
          if (isFirstRow) {
            int numColumns = row.getLastCellNum();
            while (row.getCell(numColumns - 1).getCellType().equals(CellType.BLANK)) {
              numColumns--;
            }
            format.setNumColums(numColumns);
          }
          if (isFirstRow) {
            if (format.hasHeader()) {
              header = getExcelRowData(row, rowNum);
            } else {
              header = getDefaultHeader();
              parts.add(parsePart(getExcelRowData(row, rowNum)));
            }
            isFirstRow = false;
          } else {
            parts.add(parsePart(getExcelRowData(row, rowNum)));
          }
          rowNum++;
        }
      }
      workbook.close();
    } catch (IOException e) {
      System.out.printf("Error: File not found \"%s\"%n", file.getAbsolutePath());
    }
    return parts;
  }

  private static String[] getExcelRowData(final Row row, final int rowNum) {
    final String[] rowData = new String[format.getNumColumns()];
    int colNum = 0;
    for (Cell cell : row) {
      CellType type = cell.getCellType();
      String value;
      switch (type) {
        case STRING:
          value = cell.getStringCellValue();
          break;
        case NUMERIC:
          double num = cell.getNumericCellValue();
          if (num == (int) num) {
            value = Integer.toString((int) num);
          } else {
            value = Double.toString(num);
          }
          break;
        case BOOLEAN:
          value = Boolean.toString(cell.getBooleanCellValue());
          break;
        case BLANK:
          value = "";
          break;
        default:
          System.err.printf("Error: unsupported cell type %s at row %d column %d",
              type.toString(), rowNum + 1, colNum + 1);
          Input.exit(1);
          value = "";
          break;
      }
      rowData[colNum++] = value;
    }
    return rowData;
  }

  private static Part parsePart(final String[] rowData) {
    final Map<String, String> fieldToAttribute = new HashMap<>();
    for (int i = 0; i < format.getNumColumns(); i++) {
      fieldToAttribute.put(header[i], rowData[i]);
    }
    return new Part(fieldToAttribute);
  }

  private static String[] getDefaultHeader() {
    String[] defaultHeader = new String[format.getNumColumns()];
    for (int col = 0; col < format.getNumColumns(); col++) {
      defaultHeader[col] = "Col".concat(Integer.toString(col + 1));
    }
    return defaultHeader;
  }

}
