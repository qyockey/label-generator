package com.limerock.labelgenerator;

import static com.limerock.labelgenerator.Constants.FONTS;
import static com.limerock.labelgenerator.Constants.PAGE_SIZES;

import java.io.FileNotFoundException;

import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.properties.HorizontalAlignment;
import com.itextpdf.layout.properties.TextAlignment;
import com.itextpdf.layout.properties.VerticalAlignment;

public class PdfGenerator {
  
  private final LabelFormat format;
  private final Document doc;
  private Table table;
  private final float marginWidth;
  private final float rowHeight;
  private final float colWidth;
  
  public PdfGenerator(final String filename, final LabelFormat format)
      throws FileNotFoundException {
    this.format = format;
    final PdfDocument pdfDoc = new PdfDocument(new PdfWriter(filename));
    marginWidth = format.getMarginWidth();
    final PageSize pageSize = PAGE_SIZES.get(format.getPageSize());
    if (format.getLandscapeMode()) {
      pageSize.rotate();
    }
    doc = new Document(pdfDoc, pageSize);
    doc.setMargins(marginWidth, marginWidth, marginWidth, marginWidth);
    // subtract 5 units from to height to get correct spacing (?)
    rowHeight = (pageSize.getHeight() - doc.getTopMargin()
        - doc.getBottomMargin()) / format.getEntriesPerCol() - 5;
    colWidth = format.getPageWidthPercent() / 100 * (pageSize.getWidth()
        - doc.getLeftMargin() - doc.getRightMargin()) / format.getEntriesPerRow() - 5;
    table = new Table(format.getColsPerEntry() * format.getEntriesPerRow())
        .setTextAlignment(TextAlignment.CENTER)
        .setHorizontalAlignment(HorizontalAlignment.CENTER)
        .setVerticalAlignment(VerticalAlignment.MIDDLE)
        .setWidth((pageSize.getWidth() - doc.getLeftMargin()
            - doc.getRightMargin()) * format.getPageWidthPercent() / 100);
  }

  public final PdfGenerator addPartRow(final Part[] partRow) {
    for (EntryCell[] row : format.getCellLayout()) {
      for (Part part : partRow) {
        for (EntryCell entryCell : row) {
          if (part == null) {
            addBlankCell();
          } else {
            switch(entryCell.getCellType()) {
              case BLANK_CELL:
                addBlankCell();
                break;
              case EMPTY_CELL: 
                addEmptyCell();
                break;
              case TEXT_CELL:
                addTextCell(entryCell.getTextData(), part);
                break;
              case QR_CELL:
                addQrCell(entryCell.getEncodedData(), part);
                break;
              case IMAGE_CELL:
                addImageCell(entryCell.getEncodedData(), part);
                break;
            }
          }
        }
      }
    }
    return this;
  }

  private void addEmptyCell() {
    final Cell emptyCell = CellFactory.createEmptyCell(rowHeight, colWidth,
        format.hasOutline());
    table.addCell(emptyCell);
  }

  private void addBlankCell() {
    final Cell blankCell = CellFactory.createEmptyCell(rowHeight, colWidth, false);
    table.addCell(blankCell);
  }

  private void addTextCell(TextCellLine[] textData, Part part) {
    final Cell textCell = CellFactory.createTextCell(textData, part, 
        FONTS.get(format.getFont()), rowHeight, colWidth, format.hasOutline());
    table.addCell(textCell);
  }

  private void addQrCell(String dataToEncode, Part part) {
    final Cell qrCell = CellFactory.createQrCell(dataToEncode, part, doc.getPdfDocument(),
        format.getImagePadding(), rowHeight, colWidth, format.hasOutline());
    table.addCell(qrCell);
  }

  private void addImageCell(String imageFilepath, Part part) {
    final Cell imageCell = CellFactory.createImageCell(imageFilepath, part,
        format.getImagePadding(), rowHeight, colWidth, format.hasOutline());
    table.addCell(imageCell);
  }

  public final PdfGenerator writeChanges() {
    doc.add(table);
    doc.close();
    return this;
  }

}
