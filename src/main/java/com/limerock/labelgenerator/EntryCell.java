package com.limerock.labelgenerator;

import com.limerock.labelgenerator.Constants.CellType;

public class EntryCell {
  private final CellType cellType;
  private final String encodedData;
  private final TextCellLine[] textData;

  public EntryCell(final CellType cellType) {
    this.cellType = cellType;
    this.encodedData = null;
    this.textData = null;
  }

  public EntryCell(final CellType cellType, final String encodedData) {
    this.cellType = cellType;
    this.encodedData = encodedData;
    this.textData = null;
  }

  public EntryCell(final CellType cellType, final TextCellLine[] textData) {
    this.cellType = cellType;
    this.encodedData = null;
    this.textData = textData;
  }

  public CellType getCellType() {
    return cellType;
  }

  public String getEncodedData() {
    return encodedData;
  }

  public TextCellLine[] getTextData() {
    return textData;
  }

}
