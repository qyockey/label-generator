package com.limerock.labelgenerator;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Input {
  private Input() {}

  private static final Scanner scanner = new Scanner(System.in);
  private static final int MESSAGE_LENGTH = 45;

  public static String readString(final String msg) {
    String padding = new String(new char[MESSAGE_LENGTH - msg.length()])
        .replace("\0", " ");
    System.out.print(padding + msg);
    return scanner.nextLine().trim();
  }

  public static String readStringFromOptions(final String msg, Set<String> options) {
    List<String> optionsList = Arrays.asList(options.toArray(new String[0]));
    while (true) {
      final String input = readString(msg);
      final List<String> insensitiveMatches = optionsList.stream()
          .filter(option -> option.toLowerCase().startsWith(input.toLowerCase()))
          .collect(Collectors.toList());
      if (insensitiveMatches.size() == 1) {
        return insensitiveMatches.get(0);
      }
      final List<String> sensitiveMatches = optionsList.stream()
          .filter(option -> option.startsWith(input))
          .collect(Collectors.toList());
      if (sensitiveMatches.size() == 1) {
        return sensitiveMatches.get(0);
      }
      final List<String> exactMatches = optionsList.stream()
          .filter(option -> option.equals(input))
          .collect(Collectors.toList());
      if (exactMatches.size() == 1) {
        return exactMatches.get(0);
      }
      System.out.printf("\"%s\" is not a valid option. Acceptable responses are: %n",
          input);
      options.stream().forEach(option -> System.out.println("  " + option));
      System.out.println("Or enter a unique sequence of characters an option starts with");
    }
  }

  public static int readInt(final String msg) {
    while (true) {
      final String input = readString(msg);
      try {
        int value = Integer.parseInt(input);
        if (value <= 0) {
          throw new NumberFormatException();
        }
        return value;
      } catch (NumberFormatException e) {
        System.out.printf("\"%s\" is not a valid positive integer, please try again%n", input);
      }
    }
  }

  public static float readFloat(final String msg) {
    while (true) {
      final String input = readString(msg);
      try {
        float value = Float.parseFloat(input);
        if (value < 0) {
          throw new NumberFormatException();
        }
        return value;
      } catch (NumberFormatException e) {
        System.out.printf("\"%s\" is not a valid non-negative number, please try again%n", input);
      }
    }
  }

  public static boolean readBool(final String msg) {
    while (true) {
      final String input = readString(msg);
      if (input.toLowerCase().startsWith("y")) {
        return true;
      } else if (input.toLowerCase().startsWith("n")) {
        return false;
      } else {
        System.out.printf("\"%s\" is not a valid response, please enter y or n%n", input);
      }
    }
  }

  public static void exit(final int errorCode) {
    readString("Press enter to exit...");
    System.exit(errorCode);
  }
  
}
