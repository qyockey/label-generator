package com.limerock.labelgenerator;

public class SpreadsheetFormat {

  public enum Extension {
    CSV,
    XLS,
    XLSX
  }

  private Extension extension = null;
  private String delimiter = null;
  private boolean hasHeader = false;
  private int numColumns = 0;

  public SpreadsheetFormat setExtension(final Extension extension) {
    this.extension = extension;
    return this;
  }

  public SpreadsheetFormat setDelimiter(final String delimiter) {
    this.delimiter = delimiter;
    return this;
  }

  public SpreadsheetFormat setHasHeader(boolean hasHeader) {
    this.hasHeader = hasHeader;
    return this;
  }

  public SpreadsheetFormat setNumColums(int numColumns) {
    this.numColumns = numColumns;
    return this;
  }

  public Extension getExtension() {
    return extension;
  }

  public String getDelimiter() {
    return delimiter;
  }

  public boolean hasHeader() {
    return hasHeader;
  }

  public int getNumColumns() {
    return numColumns;
  }

}
