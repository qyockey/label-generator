package com.limerock.labelgenerator;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.itextpdf.io.font.constants.StandardFonts;
import com.itextpdf.kernel.font.PdfFont;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;

public class Constants {
  private Constants() {}

  public static final float PT_PER_INCH = 72;

  public enum CellType {
    BLANK_CELL,
    EMPTY_CELL,
    TEXT_CELL,
    QR_CELL,
    IMAGE_CELL,
  }

  public static final Map<String, CellType> CELL_TYPES;
  public static final Map<CellType, String> CELL_TYPE_NAMES;
  public static final Map<String, PdfFont> FONTS;
  public static final Map<String, PageSize> PAGE_SIZES;

  static {
    Map<String, CellType> cellTypes = new LinkedHashMap<>();
    cellTypes.put("Text",  CellType.TEXT_CELL);
    cellTypes.put("QR",    CellType.QR_CELL);
    cellTypes.put("Image", CellType.IMAGE_CELL);
    cellTypes.put("Blank", CellType.BLANK_CELL);
    cellTypes.put("Empty", CellType.EMPTY_CELL);
    CELL_TYPES = Collections.unmodifiableMap(cellTypes);

    Map<CellType, String> cellTypeNames = new LinkedHashMap<>();
    cellTypeNames.put(CellType.TEXT_CELL,  "Text");
    cellTypeNames.put(CellType.QR_CELL,    "QR");
    cellTypeNames.put(CellType.IMAGE_CELL, "Image");
    cellTypeNames.put(CellType.BLANK_CELL, "Blank");
    cellTypeNames.put(CellType.EMPTY_CELL, "Empty");
    CELL_TYPE_NAMES = Collections.unmodifiableMap(cellTypeNames);

    Map<String, PdfFont> fonts = new LinkedHashMap<>();
    try {
      fonts.put("Courier",   PdfFontFactory.createFont(StandardFonts.COURIER));
      fonts.put("Helvetica", PdfFontFactory.createFont(StandardFonts.HELVETICA));
      fonts.put("Times",     PdfFontFactory.createFont(StandardFonts.TIMES_ROMAN));
    } catch (IOException e) {
      System.out.println("Error: unable to load fonts");
    }
    FONTS = Collections.unmodifiableMap(fonts);

    Map<String, PageSize> pageSizes = new LinkedHashMap<>();
    pageSizes.put("Executive", PageSize.EXECUTIVE);
    pageSizes.put("Ledger",    PageSize.LEDGER);
    pageSizes.put("Legal",     PageSize.LEGAL);
    pageSizes.put("Letter",    PageSize.LETTER);
    pageSizes.put("Tabloid",   PageSize.TABLOID);
    PAGE_SIZES = Collections.unmodifiableMap(pageSizes);
  }
}
