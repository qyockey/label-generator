package com.limerock.labelgenerator;

import static com.limerock.labelgenerator.Constants.CELL_TYPES;
import static com.limerock.labelgenerator.Constants.CELL_TYPE_NAMES;
import static com.limerock.labelgenerator.Constants.PT_PER_INCH;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.limerock.labelgenerator.Constants.CellType;

public class LabelFormat {

  private Map<String, Object> fields = new LinkedHashMap<>();
  private int lineNum = 0;

  private static final String FONT = "font";
  private static final String PAGE_SIZE = "pageSize";
  private static final String LANDSCAPE_MODE = "landscapeMode";
  private static final String HAS_OUTLINE = "hasOutline";
  private static final String CELL_LAYOUT = "cellLayout";
  private static final String ROWS_PER_ENTRY = "rowsPerEntry";
  private static final String COLS_PER_ENTRY = "colsPerEntry";
  private static final String ENTRIES_PER_ROW = "entriesPerRow";
  private static final String ENTRIES_PER_COL = "entriesPerCol";
  private static final String PAGE_WIDTH_PERCENT = "pageWidthPercent";
  private static final String MARGIN_WIDTH = "marginWidth";
  private static final String IMAGE_PADDING = "imagePadding";

  public LabelFormat() {}

   public LabelFormat(final File cfgFile) {
    try (final BufferedReader reader = new BufferedReader(new FileReader(cfgFile))) {
      String line;
      while ((line = readCfgLine(reader)) != null) {
        final String[] tokens = line.split("\\s*=\\s*");
        if (tokens.length != 2) {
          System.out.printf("Error: Invalid line \"%s\" in %s config file%n",
              line, cfgFile.getName());
          System.out.println("Lines must conist of the format \"field = value\"");
          throw new IllegalArgumentException();
        }
        final String field = tokens[0];
        String valueStr = tokens[1];
        final Object value;
        if (field.equals(CELL_LAYOUT)) {
          value = parseCellLayout(reader, getRowsPerEntry(), getColsPerEntry());
        } else {
          value = parseValue(field, valueStr);
        }
        setField(field, value);
      }
    } catch (IOException e) {
      System.err.printf("Error reading config file at line %d", lineNum);
      System.err.println("Please make sure the file exists and has read permissions");
      Input.exit(1);
    }
  }

  private String readCfgLine(final BufferedReader reader) {
    lineNum++;
    try {
      return reader.readLine();
    } catch (IOException e) {
      System.err.printf("Error reading config file at line %d", lineNum);
      System.err.println("Please make sure the file exists and has read permissions");
      Input.exit(1);
      return null;
    }
  }

  private Object parseValue(final String field, final String valueStr) {
    switch (field) {
      case FONT:
      case PAGE_SIZE:
        return valueStr;
      case LANDSCAPE_MODE:
      case HAS_OUTLINE:
        return Boolean.parseBoolean(valueStr);
      case ROWS_PER_ENTRY:
      case COLS_PER_ENTRY:
      case ENTRIES_PER_ROW:
      case ENTRIES_PER_COL:
        return toInt(valueStr);
      case PAGE_WIDTH_PERCENT:
      case MARGIN_WIDTH:
      case IMAGE_PADDING:
        return Float.parseFloat(valueStr);
      default:
        System.err.printf("Error: Invalid paramater \"%s\" in config file at line %d%n",
            field, lineNum);
        Input.exit(1);
        return null;
    }
  }

  private EntryCell[][] parseCellLayout(
      final BufferedReader reader,
      final int rowsPerEntry,
      final int colsPerEntry
  ) {
    EntryCell[][] cellLayout = new EntryCell[rowsPerEntry][colsPerEntry];
    final String fieldNumber = "(\\d+)\\s*=";
    final String value = "=\\s*(\\S(.*\\S)?)";
    final String blockEnd = "\\s*\\}\\s*";
    String line = readCfgLine(reader); // start of row
    do {
      final String rowNumStr = getRegexMatch(line, fieldNumber);
      final int rowNum = toInt(rowNumStr) - 1;
      line = readCfgLine(reader); // start of column
      do {
        final String colNumStr = getRegexMatch(line, fieldNumber);
        final int colNum = toInt(colNumStr) - 1;
        final String cellTypeStr = getRegexMatch(readCfgLine(reader), value);
        final CellType cellType = CELL_TYPES.get(cellTypeStr);
        if (cellType == null) {
          System.err.printf("Error: Invalid cell type \"%s\" at line %d%n",
              cellTypeStr, lineNum);
          Input.exit(1);
        }
        switch (cellType) {
          case BLANK_CELL:
          case EMPTY_CELL:
            cellLayout[rowNum][colNum] = new EntryCell(cellType);
            break;
          case TEXT_CELL:
            final List<TextCellLine> textData = new ArrayList<>();
            readCfgLine(reader); // start of text data block
            readCfgLine(reader); // start of line block
            do {
              final String contents = getRegexMatch(readCfgLine(reader), value);
              final String isBoldStr = getRegexMatch(readCfgLine(reader), value);
              final boolean isBold = Boolean.parseBoolean(isBoldStr);
              final String isItalicStr = getRegexMatch(readCfgLine(reader), value);
              final boolean isItalic = Boolean.parseBoolean(isItalicStr);
              textData.add(new TextCellLine(contents, isBold, isItalic));
              readCfgLine(reader); // end of line block
              line = readCfgLine(reader); // start of next line, or end of textData
            } while (!line.matches(blockEnd));
            cellLayout[rowNum][colNum] = new EntryCell(cellType,
                textData.toArray(new TextCellLine[0]));
            readCfgLine(reader); // end of textData
            break;
          case QR_CELL:
          case IMAGE_CELL:
            final String contents = getRegexMatch(readCfgLine(reader), value);
            cellLayout[rowNum][colNum] = new EntryCell(cellType, contents);
            break;
        }
        line = readCfgLine(reader); // start of next cell, or end of row
      } while (!line.matches(blockEnd));
      line = readCfgLine(reader); // start of next row, or end of layout
    } while (!line.matches(blockEnd));
    readCfgLine(reader); // end of rows
    return cellLayout;
  }

  private int toInt(final String str) {
    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException e) {
      System.err.printf("Error at line %d: \"%s\" is not a valid integer%n",
          lineNum, str);
      Input.exit(1);
      return Integer.MIN_VALUE;
    }
  }

  private String getRegexMatch(
      final String str,
      final String regex
  ) {
    final Pattern pattern = Pattern.compile(regex);
    final Matcher matcher = pattern.matcher(str);
    if (matcher.find()) {
      return matcher.group(1);
    }
    System.err.printf("Error near line %d%n", lineNum);
    Input.exit(1);
    return null;
  }

  public void writeConfigFile(final File cfgFile) {
    try (final BufferedWriter writer = new BufferedWriter(new FileWriter(cfgFile))) {
      for (Map.Entry<String, Object> entry : fields.entrySet()) {
        final String field = entry.getKey();
        final Object value = entry.getValue();
        switch (field) {
          case FONT:
          case PAGE_SIZE:
            writeFieldOption(field, (String) value, writer);
            break;
          case LANDSCAPE_MODE:
          case HAS_OUTLINE:
            writeFieldOption(field, Boolean.toString((boolean) value), writer);
            break;
          case CELL_LAYOUT:
            writeCellLayout(field, (EntryCell[][]) value, writer);
            break;
          case ROWS_PER_ENTRY:
          case COLS_PER_ENTRY:
          case ENTRIES_PER_ROW:
          case ENTRIES_PER_COL:
            writeFieldOption(field, Integer.toString((int) value), writer);
            break;
          case PAGE_WIDTH_PERCENT:
          case MARGIN_WIDTH:
          case IMAGE_PADDING:
            writeFieldOption(field, Float.toString((float) value), writer);
            break;
          default:
            writeFieldOption(field, (String) value, writer);
        }
      }
    } catch (IOException e) {
      System.err.printf("Error writing to config file \"%s\"%n", 
          cfgFile.getAbsolutePath());
      System.err.println("Please make sure the file exists and has write permissions");
      Input.exit(1);
    }
  }

  private void writeCfgLine(final BufferedWriter writer, final String line) {
    lineNum++;
    try {
      writer.write(line);
      writer.newLine();
    } catch (IOException e) {
      System.err.printf("Error writing to config file at line %d", lineNum);
      System.err.println("Please make sure the file exists and has write permissions");
      Input.exit(1);
    }
  }

  private void writeFieldOption(
      final String field,
      final String value,
      final BufferedWriter writer
  ) {
    writeCfgLine(writer, field + " = " + value);
  }

  private void writeCellLayout(
      final String field,
      final EntryCell[][] cellLayout,
      final BufferedWriter writer
  ) throws IOException {
    writeFieldOption(field, "{", writer);
    int rowNum = 1;
    for (EntryCell[] row : cellLayout) {
      writeFieldOption("  row" + Integer.toString(rowNum), "{", writer);
      int colNum = 1;
      for (EntryCell cell : row) {
        writeFieldOption("    col" + Integer.toString(colNum), "{", writer);
        CellType cellType = cell.getCellType();
        writeFieldOption("      cellType", CELL_TYPE_NAMES.get(cellType), writer);
        switch (cellType) {
          case TEXT_CELL:
            int lineItemNum = 1;
            writeFieldOption("      textData", "{", writer);
            for (TextCellLine line : cell.getTextData()) {
              writeFieldOption("        line" + Integer.toString(lineItemNum), "{", writer);
              writeFieldOption("          contents", line.getRawData(), writer);
              writeFieldOption("          isBold", Boolean.toString(line.isBold()), writer);
              writeFieldOption("          isItalic", Boolean.toString(line.isItalic()), writer);
              lineItemNum++;
              writeCfgLine(writer, "        }");
            }
            writer.write("      }");
            writer.newLine();
            break;
          case QR_CELL:
          case IMAGE_CELL:
            writeFieldOption("      encodedData", cell.getEncodedData(), writer);
            break;
          default:
            break;
        }
        writeCfgLine(writer, "    }");
        colNum++;
      }
      writeCfgLine(writer, "  }");
      rowNum++;
    }
    writeCfgLine(writer, "}");
  }

  private void setField(final String field, final Object value) {
    if (fields.containsKey(field)) {
      fields.replace(field, value);
    } else {
      fields.put(field, value);
    }
  }

  public LabelFormat setFont(final String font) {
    setField(FONT, font);
    return this;
  }

  public LabelFormat setPageSize(final String pageSize) {
    setField(PAGE_SIZE, pageSize);
    return this;
  }

  public LabelFormat setLandscapeMode(final boolean landscapeMode) {
    setField(LANDSCAPE_MODE, landscapeMode);
    return this;
  }

  public LabelFormat setHasOutline(final boolean hasOutline) {
    setField(HAS_OUTLINE, hasOutline);
    return this;
  }

  public LabelFormat setCellLayout(final EntryCell[][] cellLayout) {
    setField(ROWS_PER_ENTRY, cellLayout.length);
    setField(COLS_PER_ENTRY, cellLayout[0].length);
    setField(CELL_LAYOUT, cellLayout);
    return this;
  }

  public LabelFormat setEntriesPerRow(final int entriesPerRow) {
    setField(ENTRIES_PER_ROW, entriesPerRow);
    return this;
  }

  public LabelFormat setEntriesPerCol(final int entriesPerCol) {
    setField(ENTRIES_PER_COL, entriesPerCol);
    return this;
  }

  public LabelFormat setPageWidthPercent(final float pageWidthPercent) {
    setField(PAGE_WIDTH_PERCENT, pageWidthPercent);
    return this;
  }

  public LabelFormat setMarginWidth(final float marginWidth) {
    setField(MARGIN_WIDTH, marginWidth);
    return this;
  }

  public LabelFormat setImagePadding(final float imagePadding) {
    setField(IMAGE_PADDING, imagePadding);
    return this;
  }

  public String getFont() {
    return (String) fields.get(FONT);
  }

  public String getPageSize() {
    return (String) fields.get(PAGE_SIZE);
  }

  public boolean getLandscapeMode() {
    return (boolean) fields.get(LANDSCAPE_MODE);
  }

  public boolean hasOutline() {
    return (boolean) fields.get(HAS_OUTLINE);
  }

  public EntryCell[][] getCellLayout() {
    return (EntryCell[][]) fields.get(CELL_LAYOUT);
  }

  public int getRowsPerEntry() {
    return (int) fields.get(ROWS_PER_ENTRY);
  }

  public int getColsPerEntry() {
    return (int) fields.get(COLS_PER_ENTRY);
  }

  public int getEntriesPerRow() {
    return (int) fields.get(ENTRIES_PER_ROW);
  }

  public int getEntriesPerCol() {
    return (int) fields.get(ENTRIES_PER_COL);
  }

  public float getPageWidthPercent() {
    return (float) fields.get(PAGE_WIDTH_PERCENT);
  }

  public float getMarginWidth() {
    return (float) fields.get(MARGIN_WIDTH) * PT_PER_INCH;
  }

  public float getImagePadding() {
    return (float) fields.get(IMAGE_PADDING) * PT_PER_INCH;
  }

}
